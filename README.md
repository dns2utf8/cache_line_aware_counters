# cache_line_aware_counters

This program is an optimized version of the assignment 2 exercise of the KV Parallel Computing (342.295) by Armin Biere / Wolfgang Schreiner, 2020S.

## Features

* Seperate workspace for each worker thread with `WorkerLocalSpace`
    * The workspaces are aligned to reside in their own cache line to boost performance even more (see `#[repr(align(64))]`)
* Atomic operations on the shared state using `AtomicUsize`
    * Using `Relaxed` and `SeqCst` ordering to minimize the overhead
    * Implement own barrier to avoid a mutex
* A fancy animation that jumps up and down in anticipation of the result (look for `anim.chars().cycle()`) 🎉

## Setup

1. Install [rustup](https://rustup.rs)
2. Clone this repository
3. Run `cargo build --release`
4. Measure the runtime with a command similar to this one: `time ./target/release/cache_line_aware_counters 4 9`

### Download CI build binary

* [Linux amd64](https://dns2utf8.gitlab.io/cache_line_aware_counters/artifacts/cache_line_aware_counters)
    * Make it executable: `chmod +x cache_line_aware_counters`

## Usage

```bash
target/release/cache_line_aware_counters <workers> <operations>
# (operations logarithmically in base 10)
```

## Expected runtime

Running the program on a AMD Ryzen 7 PRO 3700U in side a Lenovo T495 laptop with the power plugged in:

```bash
cargo run --release -- 4 9
    Finished release [optimized] target(s) in 0.01s
     Running `target/release/cache_line_aware_counters 4 9`
starting 4 threads for 100000000 operations
🎉 100% ...
Runtime: 300.231349ms
```
