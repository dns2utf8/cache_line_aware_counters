use std::sync::atomic::{AtomicUsize, Ordering};
use std::io::Write;
use std::time::{Duration, Instant};

// 64 Byte <- size of cache line
#[repr(align(64))]
#[derive(Debug, Default)]
struct WorkerLocalSpace {
    local_progress: AtomicUsize,
}

impl WorkerLocalSpace {
    fn new() -> WorkerLocalSpace {
        WorkerLocalSpace {
            local_progress: AtomicUsize::new(0),
        }
    }
}

static GO: AtomicUsize = AtomicUsize::new(0);

fn main() {
    // poor mans argument parsing
    let mut args = std::env::args();
    let my_name = args.next().unwrap();
    let usage = format!(
        "usage: {} <workers> <operations>\n(operations logarithmically in base 10)",
        my_name
    );
    let n_workers: usize = args.next().expect(&usage).parse().expect(&usage);
    let base_operations: usize = args.next().expect(&usage).parse().expect(&usage);

    let max_operations = (0..base_operations).fold(1, |b, _e| b * 10);

    println!(
        "starting {} threads for {} operations",
        n_workers, max_operations
    );

    let pool = threadpool::Builder::new()
        .num_threads(n_workers + 1)
        .build();

    let mut workspaces = vec![];
    workspaces.resize_with(n_workers, WorkerLocalSpace::new);

    GO.store(n_workers + 2, Ordering::SeqCst);

    {
        let workspaces = unsafe {
            std::mem::transmute::<&Vec<WorkerLocalSpace>, &'static Vec<WorkerLocalSpace>>(
                &workspaces,
            )
        };
        pool.execute(move || {
            spin_await_go();
            progress_thread(max_operations, workspaces).expect("unable to display progress");
        });
    }

    let operations_per_worker = max_operations / n_workers;
    for workspace in &workspaces {
        let workspace = unsafe {
            std::mem::transmute::<&WorkerLocalSpace, &'static WorkerLocalSpace>(workspace)
        };
        pool.execute(move || {
            spin_await_go();
            worker(operations_per_worker, workspace);
        });
    }

    spin_await_go();
    let start = Instant::now();

    pool.join();

    let time = start.elapsed();

    println!("Runtime: {:?}", time);
}

fn progress_thread(local_max: usize, workspaces: &Vec<WorkerLocalSpace>) -> std::io::Result<()> {
    let delay = Duration::from_millis(100);

    let stdout = std::io::stdout();
    let mut handle = stdout.lock();

    let anim = "..|''|..";
    let mut anim = anim.chars().cycle();

    let mut local_result;

    while {
        local_result = sum_workers_operations(workspaces);

        update_progress_bar(
            &mut handle,
            anim.next().unwrap(),
            local_result * 100 / local_max,
        )?;
        std::thread::sleep(delay);

        local_result < local_max
    } {}

    handle.write_all(b"\n")
}

fn update_progress_bar(
    handle: &mut std::io::StdoutLock,
    anim: char,
    percent: usize,
) -> std::io::Result<()> {
    let anim = if percent < 100 { anim } else { '🎉' };
    let s = format!("\r{} {}% ...", anim, percent);

    handle.write_all(s.as_bytes())?;
    handle.flush()
}

fn worker(max_operations: usize, workspace: &WorkerLocalSpace) {
    for i in 0..=max_operations {
        workspace.local_progress.store(i, Ordering::Relaxed);
    }
}

fn sum_workers_operations(workspaces: &Vec<WorkerLocalSpace>) -> usize {
    workspaces
        .iter()
        .map(|w| w.local_progress.load(Ordering::Relaxed))
        .sum()
}

fn spin_await_go() {
    GO.fetch_sub(1, Ordering::Relaxed);

    while GO.load(Ordering::SeqCst) > 0 {}
}
